<?php

$lang['reports_app_description'] = 'Додаток «Базові звіти» надає набір стандартних інструментів звітів для операційної системи.';
$lang['reports_app_name'] = 'Базові звіти';
$lang['reports_date_range'] = 'Проміжок часу';
$lang['reports_device_type_summary'] = 'Device Type Summary';
$lang['reports_device_types'] = 'Типи пристроїв';
$lang['reports_entries'] = 'Записи';
$lang['reports_full_report'] = 'Детальний звіт';
$lang['reports_ip_summary'] = 'IP Summary';
$lang['reports_last_30_days'] = 'Останні 30 днів';
$lang['reports_last_7_days'] = 'Останні 7 днів';
$lang['reports_live'] = 'Live';
$lang['reports_range_invalid'] = 'Діапазон недійсний.';
$lang['reports_report_data'] = 'Дані звіту';
$lang['reports_report_settings'] = 'Налаштування звіту';
$lang['reports_reports'] = 'Звіти';
$lang['reports_today'] = 'Сьогодні';
$lang['reports_top_device_types'] = 'Топ типів пристроїв';
$lang['reports_top_external_ips'] = 'Топ зовнішніх IP-адрес';
$lang['reports_top_ips'] = 'Top IPs';
$lang['reports_top_users'] = 'Топ користувачів';
$lang['reports_user_summary'] = 'User Summary';
$lang['reports_yesterday'] = 'Вчора';
